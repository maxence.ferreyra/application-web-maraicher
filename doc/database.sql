set schema 'groupe-marche';
set role 'groupe-marche';

DROP TABLE IF EXISTS "Participation", "Personne", "Produit", "SituationVente", "Transaction", "Unite", "Utilisateur", "Vendeur", "Vente";

-- Table Personne --

CREATE TABLE "Personne" (
    id_personne SERIAL,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    date_naissance DATE,
    sexe CHAR(1),

PRIMARY KEY (id_personne)
);

-- Table Utilisateur --

CREATE TABLE "Utilisateur" (
    id_utilisateur INTEGER,
    nom_utilisateur VARCHAR(50) UNIQUE,
    mot_de_passe VARCHAR(50),

PRIMARY KEY (id_utilisateur),
FOREIGN KEY (id_utilisateur) REFERENCES "Personne" (id_personne)
);

-- Table Participation --

CREATE TABLE "Participation" (
    id_vendeur_fk INTEGER,
    id_situation_vente_fk INTEGER,
    date_participation DATE,

    PRIMARY KEY(id_vendeur_fk, id_situation_vente_fk, date_participation)
);

-- Table Vendeur --

CREATE TABLE "Vendeur" (
    id_vendeur INTEGER,
    matricule INT UNIQUE,

PRIMARY KEY(id_vendeur),
FOREIGN KEY(id_vendeur) REFERENCES "Personne" (id_personne)
);

-- Table SituationVente --

CREATE TABLE "SituationVente" (
    id_situation_vente SERIAL,
    rue VARCHAR(50),
    ville VARCHAR(50),
    CP INT,
    nom_marche VARCHAR(50),

    PRIMARY KEY(id_situation_vente)
);

-- Table Vente --

CREATE TABLE "Vente" (
    id_vente SERIAL,
    id_utilisateur_fk INTEGER,

    PRIMARY KEY(id_vente),
    FOREIGN KEY (id_utilisateur_fk) REFERENCES "Utilisateur" (id_utilisateur)
);


-- Table Unite --

CREATE TABLE "Unite" (
    id_unite SERIAL,
    nom VARCHAR(25),

PRIMARY KEY(id_unite)
);

-- Table Produit --

CREATE TABLE "Produit" (
    id_produit SERIAL,
    prix_unitaire FLOAT,
    nom VARCHAR(50),
    description TEXT, 
    categorie VARCHAR(7),
    id_unite_fk INT,

    PRIMARY KEY(id_produit),
    FOREIGN KEY (id_unite_fk) REFERENCES "Unite" (id_unite)
);

-- Table Transaction --

CREATE TABLE "Transaction" (
    id_transaction SERIAL,
    date DATE,
    quantite INT,
    remise INT,
    id_vendeur_fk INT,
    id_produit_fk INT,
    id_vente_fk INT,

PRIMARY KEY(id_transaction),
FOREIGN KEY(id_vendeur_fk) REFERENCES "Vendeur" (id_vendeur),
FOREIGN KEY(id_produit_fk) REFERENCES "Produit" (id_produit),
FOREIGN KEY(id_vente_fk) REFERENCES "Vente" (id_vente)
);


ALTER TABLE "Participation" ADD CONSTRAINT participation_vendeur_key FOREIGN KEY (id_vendeur_fk) REFERENCES "Vendeur" (id_vendeur);
ALTER TABLE "Participation" ADD CONSTRAINT participation_situation_vente_key FOREIGN KEY (id_situation_vente_fk) REFERENCES "SituationVente" (id_situation_vente);


INSERT INTO "Unite" (nom) VALUES ('Grammes');
INSERT INTO "Unite" (nom) VALUES ('Unitaire');
INSERT INTO "Unite" (nom) VALUES ('Kilogrammes');

INSERT INTO "Personne" (nom,prenom,date_naissance,sexe) VALUES ('Gerard','Godet','1923-02-06','H');
INSERT INTO "Personne" (nom,prenom,date_naissance,sexe) VALUES ('Dubois','Sandra','1975-08-16','F');
INSERT INTO "Personne" (nom,prenom,date_naissance,sexe) VALUES ('Delamer','Patrick','1983-02-06','H');
INSERT INTO "Personne" (nom,prenom,date_naissance,sexe) VALUES ('Nono','Lila','2000-03-06','F');
INSERT INTO "Personne" (nom,prenom,date_naissance,sexe) VALUES ('hgdofhd','gsdst','1922-02-06','F');

INSERT INTO "Vendeur" (id_vendeur, matricule) VALUES (2, 25);
INSERT INTO "Vendeur" (id_vendeur, matricule) VALUES (1, 17);

INSERT INTO "Utilisateur" (id_utilisateur, nom_utilisateur, mot_de_passe) VALUES (1, 'gegod', 'P@ssword');
INSERT INTO "Utilisateur" (id_utilisateur, nom_utilisateur, mot_de_passe) VALUES (3, 'Patoche', 'P@ssword');
INSERT INTO "Utilisateur" (id_utilisateur, nom_utilisateur, mot_de_passe) VALUES (4, 'Nono', 'P@ssword');



